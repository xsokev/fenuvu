import {name} from '@/../app.config';

const storage = {
  getBoolean: (key) => {
    const v = localStorage.getItem(`${name || ''}-${key}`);
    return v === "true";
  },
  getNumber: (key) => {
    const v = localStorage.getItem(`${name || ''}-${key}`);
    return v ? Number(v) : null;
  },
  getString: (key) => {
    return localStorage.getItem(`${name || ''}-${key}`);
  },
  set: (key, token) => {
    localStorage.setItem(`${name || ''}-${key}`, JSON.stringify(token));
  },
  remove: () => {
    localStorage.removeItem(`${name || ''}-${key}`);
  }
}
export default storage;
