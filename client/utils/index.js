import Cookies, {parse as parseCookie} from "./cookies";
import Storage from './storage';
import feathersStorage from './feathers-storage';

export {
  Cookies,
  Storage,
  feathersStorage,
  parseCookie
}