import Cookie from 'js-cookie';
import {app} from '@/../app.config';

const name = app.toLowerCase();

export const parse = (request) => {
  const cookies = request && request.headers ? request.headers.cookie : null;
  const list = {};

  if (cookies) {
    cookies.split(';').forEach((cookie) => {
      const parts = cookie.split('=');
      list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
  }

  return list;
}

const cookies = {
  get: (req, key, noNamespace) => {
    const cookies = parse(req);
    const namekey = noNamespace ? key : `${name || ''}-${key}`;
    return cookies[namekey];
  },
  getBoolean: (req, key) => {
    const v = cookies.get(req, key);
    return v === "true";
  },
  getNumber: (req, key) => {
    const v = cookies.get(req, key);
    return v ? Number(v) : null;
  },
  getString: (req, key) => {
    return cookies.get(req, key);
  },
  set: (key, value, noNamespace) => {
    const namekey = noNamespace ? key : `${name || ''}-${key}`;
    Cookie.set(namekey, JSON.stringify(value))
  },
  remove: (key, noNamespace) => {
    const namekey = noNamespace ? key : `${name || ''}-${key}`;
    Cookie.remove(namekey);
  }
}
export default cookies;
