import Vue from 'vue';
import {
  Button,
  Select,
  Alert,
  Form,
  FormItem,
  Input,
  Icon,
  Dropdown,
  DropdownMenu,
  DropdownItem,
} from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import 'element-ui/lib/theme-chalk/reset.css';
import 'element-ui/lib/theme-chalk/index.css';

locale.use(lang);
Vue.component(Alert.name, Alert);
Vue.component(Button.name, Button);
Vue.component(Dropdown.name, Dropdown);
Vue.component(DropdownMenu.name, DropdownMenu);
Vue.component(DropdownItem.name, DropdownItem);
Vue.component(Form.name, Form);
Vue.component(FormItem.name, FormItem);
Vue.component(Input.name, Input);
Vue.component(Icon.name, Icon);
Vue.component(Select.name, Select);
