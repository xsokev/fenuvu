export const SET_QUOTES = 'SET_QUOTES';

export const state = () => ({
  items: []
})
export const mutations = {
  [SET_QUOTES] (state, payload) {
    state.items = payload;
  }
}
export const actions = {
  set({ commit }, quotes) { // eslint-disable-line consistent-return
    commit(SET_QUOTES, quotes);
  }
}
export const getters = {
  random: function ({items}){
    const quoteCount = items.length;
    const quote = items[Math.floor(Math.random() * quoteCount)];
    return quote ? quote.quote : '';
  }
}
