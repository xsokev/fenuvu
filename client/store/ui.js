import Storage from '@/utils/storage';

export const TOGGLE_COLLAPSED   = "TOGGLE_COLLAPSED"
export const SET_COLLAPSED      = "SET_COLLAPSED"
export const SET_CURRENT_ROUTE  = "SET_CURRENT_ROUTE"

export const state = () => ({
  collapsed: false,
  currentRoute: ''
})
export const mutations = {
  [SET_CURRENT_ROUTE] (state, payload) {
    state.currentRoute = payload
  },
  [SET_COLLAPSED] (state, payload) {
    state.collapsed = payload
  }
}
export const actions = {
  setCurrentRoute: function ({commit}, route){
    commit(SET_CURRENT_ROUTE, route)
  },
  setCollapsed: function ({commit}, {isCollapsed, skipStorage}){
    commit(SET_COLLAPSED, isCollapsed)
    if(!skipStorage){
      Storage.set('collapsed', isCollapsed)
    }
  },
  toggleCollapsed: function ({dispatch, state}){
    dispatch('setCollapsed', {isCollapsed: !state.collapsed})
  }
}

// export const ping = () => {
//   return get(API_PING).then(data => data)
// }
