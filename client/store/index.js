import feathers from '@/api';
import { Cookies } from '@/utils';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const CLEAR_CURRENT_USER = 'CLEAR_CURRENT_USER';

export const state = () => ({
  user: null
})
export const mutations = {
  [SET_CURRENT_USER] (state, payload) {
    state.user = payload;
  },
  [CLEAR_CURRENT_USER] (state) {
    state.user = null;
  }
}
export const actions = {
  async nuxtServerInit({ commit, dispatch }, context) { // eslint-disable-line consistent-return
    const ckToken = Cookies.get(context.req, 'feathers-jwt', true);
    const ckCollapsed = Cookies.getBoolean(context.req, 'collapsed');

    if (ckToken) {
      try {
        const response = await feathers.authenticate({ strategy: 'jwt', accessToken: ckToken })
        const payload = await feathers.passport.verifyJWT(response.accessToken);
        const user = await feathers.service('users').get(payload.userId);
        feathers.set('user', user);
        commit(SET_CURRENT_USER, user);
      } catch (e) { //if token exists but error, the token is likely expired
        context.res.clearCookie('feathers-jwt')
        Cookies.remove('feathers-jwt', true);
        dispatch('logout');
      }
    }

    let isCollapsed = Cookies.getBoolean(context.req, 'collapsed');
    dispatch('ui/setCollapsed', {isCollapsed, skipStorage: true});

    const quoteData = await feathers.service('api/quotes').find();
    dispatch('quotes/set', quoteData.data);
  },
  async login({ commit }, { username, password }) {
    const response = await feathers.authenticate({strategy: 'local', username, password})
    const payload = await feathers.passport.verifyJWT(response.accessToken);
    const user = await feathers.service('users').get(payload.userId);

    feathers.set('user', user);
    commit(SET_CURRENT_USER, user);
  },
  async logout({ commit }) {
    feathers.logout().then(() => {
      commit(CLEAR_CURRENT_USER);
    });
  }
}
export const getters = {
  isAuthenticated: function ({user}){
    return user !== null;
  },
  fullName: function ({user}){
    return user ? `${user.firstname} ${user.lastname}` : '';
  },
  isAdmin: function ({user}){
    return user && user.role && user.role.name ? user.role.name.toLowerCase() === 'admin' : false;
  },
  lastLogin: function ({user}){
    return user && user.lastLogin ? "Last Login: " + user.lastLogin : "";
  }
}
