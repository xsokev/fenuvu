import feathers from '@feathersjs/feathers';
import rest from '@feathersjs/rest-client';
import auth from '@feathersjs/authentication-client';
import axios from 'axios';
import feathersStorage from '@/utils/feathers-storage';

const restClient = rest('http://localhost:3030');
const axiosInstance = axios.create({
  timeout: 1000,
  headers: {'Accept': 'application/json'}
});

const api = feathers()
  .configure(restClient.axios(axios))
  .configure(auth({ storage: feathersStorage }));

export default api;
