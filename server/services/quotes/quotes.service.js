// Initializes the `quotes` service on path `/quotes`
const createService = require('feathers-nedb');
const createModel = require('../../models/quotes.model');
const hooks = require('./quotes.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'quotes',
    Model,
    paginate: {
      "default": 100,
      "max": 100
    }
    // paginate
  };

  // Initialize our service with any options it requires
  app.use('/api/quotes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/quotes');
  service.find().then(({data}) => {
    if(data && data.length === 0){
      const _quotes = require('./quotes.json');
      if(_quotes){
        _quotes.forEach(q => {
          service.create({ _id: q.id, quote: q.joke });
        });
        console.log(`Initialized with ${_quotes.length} quote${_quotes.length === 1 ? '' : 's'}`);
      }
    } else {
      console.log(`There are ${data.length} quote${data.length === 1 ? '' : 's'}`);
    }
  })

  service.hooks(hooks);
};
