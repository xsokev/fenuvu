const users = require('./users/users.service.js');
const quotes = require('./quotes/quotes.service.js');
module.exports = function (app) {
  app.configure(users);
  app.configure(quotes);
};
