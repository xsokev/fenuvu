const assert = require('assert');
const app = require('../../server/app');

describe('\'quotes\' service', () => {
  it('registered the service', () => {
    const service = app.service('quotes');

    assert.ok(service, 'Registered the service');
  });
});
