const path = require('path');
const appInfo = require('./app.config');

module.exports = {
  loading: {
    color: '#DE9706',
  },
  srcDir: path.resolve(__dirname, 'client'),
  rootDir: path.resolve(__dirname),
  dev: process.env.NODE_ENV !== 'production',
  head: {
    titleTemplate: appInfo.title,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: appInfo.title },
    ],
  },
  css: [
    '~/styles/main.scss',
    '~/styles/icomoon/style.css',
    'element-ui/lib/theme-chalk/index.css'
  ],
  build: {
    babel: {
      plugins: [
        [
          'component',
          {
            libraryName: 'element-ui',
            styleLibraryName: 'theme-chalk'
          }
        ]
      ]
    },
    extend (config, {isDev, isClient}) {
      if(isDev && isClient){
        
      }
    }
  },
  plugins: [
    '~/plugins/element-ui'
  ]
};