import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _928a4e16 = () => import('../client/pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)
const _3f7e6507 = () => import('../client/pages/app/index.vue' /* webpackChunkName: "pages/app/index" */).then(m => m.default || m)
const _d1fa03a0 = () => import('../client/pages/about.vue' /* webpackChunkName: "pages/about" */).then(m => m.default || m)
const _111ecf0a = () => import('../client/pages/auth/sign-in.vue' /* webpackChunkName: "pages/auth/sign-in" */).then(m => m.default || m)
const _57c0984d = () => import('../client/pages/auth/register.vue' /* webpackChunkName: "pages/auth/register" */).then(m => m.default || m)
const _43b37f68 = () => import('../client/pages/auth/sign-out.vue' /* webpackChunkName: "pages/auth/sign-out" */).then(m => m.default || m)



const scrollBehavior = (to, from, savedPosition) => {
  // SavedPosition is only available for popstate navigations.
  if (savedPosition) {
    return savedPosition
  } else {
    let position = {}
    // If no children detected
    if (to.matched.length < 2) {
      // Scroll to the top of the page
      position = { x: 0, y: 0 }
    }
    else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
      // If one of the children has scrollToTop option set to true
      position = { x: 0, y: 0 }
    }
    // If link has anchor, scroll to anchor by returning the selector
    if (to.hash) {
      position = { selector: to.hash }
    }
    return position
  }
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/",
			component: _928a4e16,
			name: "index"
		},
		{
			path: "/app",
			component: _3f7e6507,
			name: "app"
		},
		{
			path: "/about",
			component: _d1fa03a0,
			name: "about"
		},
		{
			path: "/auth/sign-in",
			component: _111ecf0a,
			name: "auth-sign-in"
		},
		{
			path: "/auth/register",
			component: _57c0984d,
			name: "auth-register"
		},
		{
			path: "/auth/sign-out",
			component: _43b37f68,
			name: "auth-sign-out"
		}
    ],
    
    
    fallback: false
  })
}
